package pl.zyper.bmi;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void check_BMIState_overweight() {
        BMI bmi = new BMIForKgM(62, 1.55);
        double bmiValue = bmi.calculate();

        assertEquals(BMIChecker.State.Overweight, BMIChecker.getState(bmiValue));
    }

    @Test
    public void check_BMIState_normal() {
        BMI bmi = new BMIForKgM(64, 1.62);

        assertEquals(BMIChecker.State.Normal, BMIChecker.getState(bmi.calculate()));
    }

    @Test
    public void check_BMIState_normal2() {
        BMI bmi = new BMIForKgM(72, 1.70);

        assertEquals(BMIChecker.State.Normal, BMIChecker.getState(bmi.calculate()));
    }

    @Test
    public void check_BMIState_underweight() {
        BMI bmi = new BMIForKgM(54, 1.71);

        assertEquals(BMIChecker.State.Underweight, BMIChecker.getState(bmi.calculate()));
    }

    @Test
    public void for_valid_BMI_metric_data() {
        BMI bmi = new BMIForKgM(60, 1.70);
        double val = bmi.calculate();
        assertEquals(20.761, val, 0.001);

    }

    @Test
    public void for_valid_BMI_imperial_data() {
        BMI bmi = new BMIForLbIn(100, 70);
        double val = bmi.calculate();
        assertEquals(14.35, val, 0.01);

    }

    @Test(expected = IllegalArgumentException.class)
    public void zero_data_throw_exception1() {
        BMI bmi = new BMIForKgM(0, 0);
        bmi.calculate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void zero_data_throw_exception2() {
        BMI bmi = new BMIForKgM(-10, 10);
        bmi.calculate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void zero_data_throw_exception3() {
        BMI bmi = new BMIForKgM(60, 0);
        bmi.calculate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void zero_data_throw_exception4() {
        BMI bmi = new BMIForLbIn(0, 0);
        bmi.calculate();
    }
}