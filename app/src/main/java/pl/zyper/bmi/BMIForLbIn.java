package pl.zyper.bmi;

/**
 * Created by zyper on 3/14/18.
 */

public class BMIForLbIn extends BMI {
    BMIForLbIn(double mass, double height)
    {
        super(mass, height);
    }

    double calculate()
    {
        if (correctMass() && correctHeight()) {
            return ((mass) / (height * height))*703;
        }
        else {
            throw new IllegalArgumentException("Invalid data");
        }
    }
}
