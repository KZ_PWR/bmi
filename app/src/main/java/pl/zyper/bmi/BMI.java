package pl.zyper.bmi;

/**
 * Created by zyper on 3/1/18.
 */

abstract public class BMI {
    /* Mass */
    protected double mass;

    /* Height */
    protected double height;

    BMI(double mass, double height) {
        this.mass = mass;
        this.height = height;
    }

    protected boolean correctMass(){
        if (mass <= 0) return false;

        return true;
    }

    protected boolean correctHeight() {
        if (height <= 0) return false;

        return true;
    }

    abstract double calculate();
}
