package pl.zyper.bmi;

import android.content.Context;

/**
 * Created by zyper on 3/29/18.
 */

public abstract class BMIAction {
    public abstract void start(Context context, double bmiValue);
}
