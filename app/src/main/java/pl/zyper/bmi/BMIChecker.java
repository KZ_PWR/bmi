package pl.zyper.bmi;

/**
 * Created by zyper on 3/29/18.
 */

class BMIChecker {
    static State getState(double bmi) {
        if (bmi < 18.5) return State.Underweight;
        if (bmi > 25) return State.Overweight;

        return State.Normal;
    }

    public enum State {
        Underweight, Normal, Overweight
    }
}
