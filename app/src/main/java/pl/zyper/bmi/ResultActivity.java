package pl.zyper.bmi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Locale;

public class ResultActivity extends AppCompatActivity {

    private static final String bundleKey = "BMI_value";

    public static void start(Context context, double value) {
        Intent starter = new Intent(context, ResultActivity.class);
        starter.putExtra(bundleKey, value);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        setTexts(getBMIValue());
    }

    private double getBMIValue() {
        Bundle extras = this.getIntent().getExtras();

        double bmiValue = 0;
        if (extras != null) {
            bmiValue = extras.getDouble(bundleKey);
        }

        return bmiValue;
    }

    /*
        Sets all activity texts based on BMI
     */
    private void setTexts(double bmiValue) {
        TextView bmiText = findViewById(R.id.bmi);
        bmiText.setText(String.format(Locale.getDefault(), "%.1f", bmiValue));

        TextView bmiStateText = findViewById(R.id.bmi_state);
        BMIChecker.State state = BMIChecker.getState(bmiValue);
        String stateStr;
        if (state == BMIChecker.State.Underweight) {
            stateStr = getString(R.string.bmi_state_underweight);
        } else if (state == BMIChecker.State.Normal) {
            stateStr = getString(R.string.bmi_state_normal);
        } else {
            stateStr = getString(R.string.bmi_state_overweight);
        }
        bmiStateText.setText(stateStr);
    }
}
