package pl.zyper.bmi;

import android.content.Context;

/**
 * Created by zyper on 3/29/18.
 */

public class BMIActionActivity extends BMIAction {

    @Override
    public void start(Context context, double bmiValue) {
        ResultActivity.start(context, bmiValue);
    }
}
