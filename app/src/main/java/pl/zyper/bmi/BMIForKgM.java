package pl.zyper.bmi;

/**
 * Created by zyper on 3/8/18.
 */

public class BMIForKgM extends BMI {
    BMIForKgM(double mass, double height)
    {
        super(mass, height);
    }

    double calculate()
    {
        if (correctMass() && correctHeight()) {
            return (mass) / (height * height);
        }
        else {
            throw new IllegalArgumentException("Invalid data");
        }
    }
}
