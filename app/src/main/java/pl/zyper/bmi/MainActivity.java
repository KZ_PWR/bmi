package pl.zyper.bmi;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    private TextView massText, heightText;
    private Switch unitsSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        massText = findViewById(R.id.text_mass);
        heightText = findViewById(R.id.text_height);
        unitsSwitch = findViewById(R.id.units_switch);

        // add listeners
        addListeners();

        // load preferences
        loadInputValues();
    }

    private void addListeners() {
        // add listener to count button
        findViewById(R.id.count_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // reads parameters from text inputs

                double mass = getEditTextAsDouble(R.id.edit_mass);
                double height = getEditTextAsDouble(R.id.edit_height);

                BMI bmi;

                // creates new bmi object based on units
                if (unitsSwitch.isChecked()) {
                    bmi = new BMIForLbIn(mass, height);
                } else {
                    bmi = new BMIForKgM(mass, height);
                }

                // specifies action as new activity action
                BMIAction action = new BMIActionActivity();

                try {
                    // calculates bmi and starts new activity
                    double bmiValue = bmi.calculate();
                    action.start(MainActivity.this, bmiValue);
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getApplicationContext(), R.string.wrong_data_msg, Toast.LENGTH_SHORT).show();
                }
            }
        });

        // add listener to unit switch
        unitsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    buttonView.setText(R.string.imperial);
                    setInputLabelsWithUnits(UnitsType.IMPERIAL);
                } else {
                    buttonView.setText(R.string.metric);
                    setInputLabelsWithUnits(UnitsType.METRIC);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_save:
                try {
                    saveInputValues();
                    Toast.makeText(getApplicationContext(), R.string.data_saved, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), R.string.wrong_data_msg, Toast.LENGTH_SHORT).show();
                }

                return true;

            case R.id.menu_about:
                AboutActivity.start(MainActivity.this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveInputValues() {
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        double massVal = getEditTextAsDouble(R.id.edit_mass);
        double heightVal = getEditTextAsDouble(R.id.edit_height);
        Boolean unitsVal = ((Switch) findViewById(R.id.units_switch)).isChecked();

        // do not save if BOTH are zeros
        if (massVal == 0 && heightVal == 0) throw new IllegalArgumentException("Zero values");

        editor.putLong(getResources().getString(R.string.text_mass), Double.doubleToLongBits(massVal));
        editor.putLong(getResources().getString(R.string.text_height), Double.doubleToLongBits(heightVal));
        editor.putBoolean(getResources().getString(R.string.units), unitsVal);
        editor.apply();
    }

    private void loadInputValues() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        double mass = Double.longBitsToDouble(sharedPref.getLong(getString(R.string.text_mass), 0));
        double height = Double.longBitsToDouble(sharedPref.getLong(getString(R.string.text_height), 0));

        boolean imperialUnits = sharedPref.getBoolean(getString(R.string.units), false);

        Log.i("INFO", "read mass: " + mass);
        Log.i("INFO", "read height: " + height);

        if (mass != 0) setEditTextAsDouble(R.id.edit_mass, mass);
        if (height != 0) setEditTextAsDouble(R.id.edit_height, height);

        if (imperialUnits) {
            setInputLabelsWithUnits(UnitsType.IMPERIAL);
        } else {
            setInputLabelsWithUnits(UnitsType.METRIC);
        }

        ((Switch) findViewById(R.id.units_switch)).setChecked(imperialUnits);
    }

    private void setInputLabelsWithUnits(UnitsType type) {
        int massUnit = type == UnitsType.METRIC ? R.string.metric_mass : R.string.imperial_mass;
        int heightUnit = type == UnitsType.METRIC ? R.string.metric_height : R.string.imperial_height;

        String inputTextFormat = "%s (%s): ";

        massText.setText(String.format(inputTextFormat,
                getString(R.string.text_mass),
                getString(massUnit)));

        heightText.setText(String.format(inputTextFormat,
                getString(R.string.text_height),
                getString(heightUnit)));
    }

    private double getEditTextAsDouble(int id)
    {
        double val = 0;
        try {
            val = Double.parseDouble(((EditText) (findViewById(id))).getText().toString());
        }
        catch (Exception e)
        {
            Log.w("PARSING ERROR", e.getMessage());
        }
        return val;
    }

    private void setEditTextAsDouble(int id, double val)
    {
        EditText text = findViewById(id);
        text.setText(String.format(Locale.getDefault(), "%.2f", val));
    }

    private enum UnitsType {
        METRIC /* kilograms, meters */,
        IMPERIAL /* pounds, inches */
    }
}

